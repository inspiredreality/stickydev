﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;


public class LockMenu : MonoBehaviour
{
        // StickyUIMenu StickyUIMenuInstance;
        UIController UIControllerInstance;
        public GameObject followMe, staticMenuLocation, UIMenu;
        // public GameObject followMe, staticMenuLocation, UIMenu, LookAtPlayerFace;
        public Quaternion lookAtRotation;
        // public Transform LookAtPlayerFace;
        public float followX, followY, followZ;
        // private bool staticUICanvas = false;
        // public bool locked = false;
        // public bool locked = false;

    void Awake()
    {
        UIControllerInstance = FindObjectOfType <UIController>();
        // Scene scene = SceneManager.GetActiveScene();
        if(UIMenu == null){
            UIMenu = GameObject.Find("UIStickyCanvas");
            UIControllerInstance.showMenu = false;
        }
        if (GameObject.Find("StaticMenuLocation") != null){
            staticMenuLocation = GameObject.Find("StaticMenuLocation");
        }
        if (GameObject.Find("followMe") != null){
            followMe = GameObject.Find("followMe");
        }        
        // if (GameObject.Find("PlayerFace") != null){
        //     LookAtPlayerFace = GameObject.Find("PlayerFace");
        // }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // //basically works, strange swivel when player moves after pause, rotation not locked
        //working better but moving heights for player messes up menu plane
        if(UIControllerInstance.staticUICanvas)
        {
            // UIMenu.transform.position = new Vector3 (0, 0, 3);
            followX = staticMenuLocation.transform.position.x;
            followY = staticMenuLocation.transform.position.y ;
            followZ = staticMenuLocation.transform.position.z ;            
            UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
            return;
        }
        else if(UIControllerInstance.showMenu)
        {
            UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
            // UIMenu.transform.TransformPoint(followX,followY,followZ);
            UIMenu.transform.rotation = lookAtRotation;
            // UIMenu.transform.rotation = Quaternion.Euler(lookAtRotation.x, lookAtRotation.y, 0.0f);
        }
        else
        {
            followX = followMe.transform.position.x;
            followY = followMe.transform.position.y ;
            followZ = followMe.transform.position.z ;
            
            lookAtRotation = followMe.transform.rotation;
        }


        // if(StickyUIMenuInstance.inMenu)
        // {
        //     UIMenu.transform.position = new Vector3 (followX ,followY ,followZ);
        //     UIMenu.transform.TransformPoint(followX,followY,followZ);
        //     UIMenu.transform.rotation = lookAtRotation;
        // }
        // else
        // {
        //     followX = mimicPlayerFaceCube.transform.position.x;
        //     followY = mimicPlayerFaceCube.transform.position.y ;
        //     followZ = mimicPlayerFaceCube.transform.position.z ;

        //     lookAtRotation = mimicPlayerFaceCube.GetComponent<Rigidbody>().rotation;
            
        //     Vector3 relativePos.transform.TransformPoint(LookAtPlayerFace.transform.position);
        //     lookAtRotation = Quaternion.LookRotation(relativePos, Vector3.up);
        // }
    }
}
