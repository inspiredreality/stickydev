﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
    public static SFXManager sfxInstance;

    void Awake()
    {
        if (sfxInstance == null)
        {
            //assign it to current object
            sfxInstance = this;
        }
        //make sure it is the current object
        else if (sfxInstance != this)
        {
            //destroy game object, we only need 1 and it already exists
            Destroy(gameObject);
        }
        //keep alive, dont destroy when scene changes
        DontDestroyOnLoad(gameObject);
    }
}
