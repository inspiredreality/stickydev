﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class GameManager : MonoBehaviour
{
    public static GameManager GMinstance;
    public GameObject ThrowablesHierarchy, LosePlane, WinPlane;
    public string totalScore, ballsLost, realTime, thisLevelName, nextLevel;
    // playingPlayer = "Brian",
    // brianScore="0", leahScore="0", ericScore="0", davidScore="0", jessScore="0", gregScore="0", guestScore="0";

    //constant or calculated at start of level
    public float stickySecStartSmall = 3, stickySecStartBig = 5, winningScore = 30;
    public int ballsLostRaw, lostBallPenalty, thisLevelIndex, startingThrowables = 0;
    //reset between levels
    public float totalScoreRaw, bigBallScore, bigCubeScore, bigCylinderScore, bigPyramidScore, 
        smallBallScore, smallCubeScore, smallCylinderScore, smallPyramidScore, realTimeRaw;
    // public VideoPlayer loserPlayer;
    // public VideoSource loserSource;
    // public VideoClip loserClip;

    public bool paused = false, WinCondition = false, LoseCondition = false;
    // public bool playing = true, paused = false, WinCondition = false, LoseCondition = false;
    //playing=false is level won, or level lost.
    private int levelCompleted;
    public bool HighScoreLevel = false, LevelLevels = false;

    void Awake()
    {
        WinCondition = false;
        if (GMinstance == null)
        {
            //assign it to current object
            GMinstance = this;
        }
        //make sure it is the current object
        else if (GMinstance != this)
        {
            //destroy game object, we only need 1 and it already exists
            Destroy(gameObject);
        }
        // //keep alive, dont destroy when scene changes
        // DontDestroyOnLoad(gameObject);

    ///Set variables that are in every scene
        //Create array of floats for scores out of all objects tagged "Ball" instead of delaring as public floats 
        if (GameObject.Find("Throwables") != null){
            ThrowablesHierarchy = GameObject.Find("Throwables");
            startingThrowables = ThrowablesHierarchy.transform.childCount;
        }
        if (GameObject.Find("LosePlane") != null){
            LosePlane = GameObject.Find("LosePlane");
            LosePlane.SetActive(false);
        }
        if (GameObject.Find("WinPlane") != null){
            WinPlane = GameObject.Find("WinPlane");
            WinPlane.SetActive(false);
        }

        //disable for High Score level - winning score, lost ball penalty
        Scene scene = SceneManager.GetActiveScene();
        if(scene.name.Contains("High Score"))
        {
            winningScore = 9999;
            lostBallPenalty = 0;
            HighScoreLevel = true;
        }
        //set winning score
        else if(scene.name.Contains("Level"))
        {
            winningScore = 30;
            lostBallPenalty = 10;
            LevelLevels = true;
        }

        //Make sure game is unpaused to start SSS or levels
        // if(paused)
        // {
            Time.timeScale = 1;
            paused = false;
        // }
    }

    void Update()
    {
        convertRawScores();

        //only check to play game if we have Throwables
        //this is basically for SSS to not trigger LoseCondition
        if(startingThrowables > 0 )
        {
            if(totalScoreRaw > winningScore)
            //dont know why >= doesnt work but moving on
            {
                WinCondition = true;
            }
            if(ballsLostRaw == startingThrowables)
            {
                LoseCondition = true;
            }
        }

        if(WinCondition)
        {
            WinPlane.SetActive(true);
            pauseGame(); 
            StartCoroutine(WinTimerStarted());
        }
        if(LoseCondition)
        {
            LosePlane.SetActive(true);
            pauseGame(); 
            StartCoroutine(LoseTimerStarted());
        }
    }

    public void pauseGame()
    {
        if(!WinCondition && !LoseCondition)
        {
            if(paused)
            {
                Time.timeScale = 1;
                paused = false;
            }
            else
            {
                Time.timeScale = 0;
                paused = true;            
            }
        }

        if(WinCondition || LoseCondition)
        {
            //use Chronos to only pause balls and not confetti 
            Time.timeScale = 0;
            paused = true;    

            Scene scene = SceneManager.GetActiveScene();
            thisLevelName = scene.name;
            // var nextLevelIndex = scene.buildIndex + 1;
            // var nextLevelIndex = scene.buildIndex + 1;

            if(HighScoreLevel)
            {
                LeaderboardTracking.LTinstance.setPlayerHighScore(Mathf.FloorToInt(totalScoreRaw));
            }
            else if(LevelLevels && WinCondition)
            {
                LeaderboardTracking.LTinstance.setPlayerFurthestLevel(scene.buildIndex - 1);
            }
        }

    }

    IEnumerator WinTimerStarted()
    {
        yield return new WaitForSeconds(4);
        WinPlane.SetActive(false);
    }
    IEnumerator LoseTimerStarted()
    {
        yield return new WaitForSeconds(4);
        LosePlane.SetActive(false);
    }

    private void convertRawScores(){
        totalScoreRaw = (bigBallScore + bigCubeScore + bigCylinderScore + bigPyramidScore + smallBallScore + 
        smallCubeScore + smallCylinderScore + smallPyramidScore) - ballsLostRaw*lostBallPenalty;
        realTimeRaw += Time.deltaTime;
        // totalScore = Mathf.FloorToInt((totalScoreRaw * 100f) % 100f);

        //send numbers as string to UI
        int ScoreSeconds = Mathf.FloorToInt(totalScoreRaw % 60f);
        int ScoreMilliseconds = Mathf.Abs(Mathf.FloorToInt((totalScoreRaw * 100f) % 100f));
        // totalScore = ScoreSeconds.ToString ("00") + "." + ScoreMilliseconds.ToString("00") + " / " + winningScore.ToString();

        if(HighScoreLevel)
        {
            totalScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        }
        else if(LevelLevels)
        {
            totalScore = Mathf.FloorToInt(totalScoreRaw).ToString() + " / " + winningScore.ToString();
        }

        ballsLost = ballsLostRaw.ToString(); 
        int realMinutes = Mathf.FloorToInt(realTimeRaw / 60f);
        int realSeconds = Mathf.FloorToInt(realTimeRaw % 60f);
        realTime = realMinutes.ToString ("00") + ":" + realSeconds.ToString ("00");

        // //assign totalScore to playerPlaying
        // switch (playingPlayer)
        // {
        //     case "Brian":
        //         brianScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "Leah":
        //         leahScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "Eric":
        //         ericScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "David":
        //         davidScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "Jess":
        //         jessScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "Greg":
        //         gregScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        //     case "Guest":
        //         guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;        
        //     default:
        //         guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
        //         break;
        // }
    }

    public void clearScores()
    {
        totalScoreRaw = bigBallScore = bigCubeScore = bigCylinderScore = bigPyramidScore = smallBallScore = 
        smallCubeScore = smallCylinderScore = smallPyramidScore = winningScore = realTimeRaw = ballsLostRaw = 0;
        paused = WinCondition = LoseCondition = false;
        convertRawScores();
    }
}
