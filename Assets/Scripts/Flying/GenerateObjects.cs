﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObjects : MonoBehaviour
{
    // public GameObject bigBallSpace, bigBallSpaceCrossfire, startInSpace, startInSpaceCrossfire, respawnArea_L, respawnArea_R, 
    public GameObject bigBallSpace, startInSpace, respawnArea_L, respawnArea_R, 
    DestroyerL, DestroyerR;
    // public GameObject bigBalls, startInSpace, respawnDestroyers, DestroyObjectShield, DestroyObjectBall;

    public int numberOfBigBalls, numberOfBigBallsCrossfire;
    // public int numberOfBigBalls, numberOfDestroyObectBalls;
    public float respawnDelayL, respawnDelayR;

    private MeshCollider col;
    // private int currentBigBallCount ;
    public bool spawnDestroyerL = false, spawnDestroyerR = false;

    public static GenerateObjects instance;
    
        void Awake()
    {
        //check that GO exists
        if (instance == null)
        {
            //assign it to current object
            instance = this;
        }
        //make sure it is the current object
        else if (instance != this)
        {
            //destroy game object, we only need 1 and it already exists
            Destroy(gameObject);
        }
        //keep alive, dont destroy when scene changes
        // DontDestroyOnLoad(gameObject);
    }

    void Start() 
    {  
        // GameObject tmp = Instantiate(terrainObject);
        // tmp.transform.position = new Vector3(0.0f, tmp.transform.position.y, 0.0f);
        // var col = startInSpace.GetComponent<MeshCollider>();

        // GenerateObject(terrainObject, numberOfTerrainObjects);
    }

    // Update is called once per frame
    void Update()
    {
        GenerateObject(bigBallSpace, startInSpace, numberOfBigBalls);
        // GenerateObject(bigBallSpaceCrossfire, startInSpaceCrossfire, numberOfBigBallsCrossfire);

        // GenerateObject(DestroyObjectBall, numberOfDEstroyObectBalls);
        if(spawnDestroyerL)
        {
            GenerateObject(DestroyerL, respawnArea_L, 1);
            spawnDestroyerL = false;
            //wait at least 1 sec
            // StartCoroutine(SpawnDestroyerTimer(1));
        }
        if(spawnDestroyerR)
        {
            GenerateObject(DestroyerR, respawnArea_R, 1);
            spawnDestroyerR = false;
        }

        
        // if(currentBigBallCount < numberOfBigBalls)
        // {
        //     GenerateObject(bigBalls, 1);
        // }
    }

    void GenerateObject(GameObject go, GameObject area, int amount)
    {
        if (go == null) return;

        for(int i = 0; i < amount; i++)
        {
            // GameObject tmp = Instantiate(go);
            // tmp.gameObject.transform.position = new Vector3(0.0f, tmp.transform.position.y, 0.0f);       
            // tmp.gameObject.transform.position = new Vector3(0.0f, 0.0f, 0.0f);       
            Vector3 randomSpawnPoint = new Vector3(0,0,0);

            randomSpawnPoint = GetRandomPoint(area);

            // if(go.name.Contains("Space")){
            //     randomSpawnPoint = GetRandomPoint(startInSpace);
            // }
            // else if (spawnDestroyerL){
            //     spawnDestroyerL = false;
            //     randomSpawnPoint = GetRandomPoint(respawnArea_L);
            // }
            // else if(spawnDestroyerR){
            //     randomSpawnPoint = GetRandomPoint(respawnArea_R);
            // }
            // else{
            //     return;
            // }
            // else if(go.name.Contains("DestroyerL")){
            //     randomSpawnPoint = GetRandomPoint(respawnDestroyerL);

            // }
            // else if(go.name.Contains("DestroyerR")){
            //     randomSpawnPoint = GetRandomPoint(respawnDestroyerR);
            // }

            GameObject tmp = Instantiate(go);
            // tmp.gameObject.transform.position = new Vector3(randomPoint.x, randomPoint.y, startInSpace.transform.position.z);
            tmp.gameObject.transform.position = new Vector3(randomSpawnPoint.x, randomSpawnPoint.y, randomSpawnPoint.z);
            // Debug.Log("instantiated at: " + randomSpawnPoint);
        }

    }

Vector3 GetRandomPoint(GameObject generateRandomPoint)
    {
        col = generateRandomPoint.GetComponent<MeshCollider>();
        // col = startInSpace.GetComponent<Collider>();
        float xRandom = 0;
        float yRandom = 0;
        float zRandom = 0;
        
        // lock (syncLock)
        //     { // synchronize
        //         return randomNum.Next(min, max);
        //     }

        xRandom = (float)Random.Range(col.bounds.min.x, col.bounds.max.x);
        //z axis on plane is translated to y axis in environment, x is same
        yRandom = (float)Random.Range(col.bounds.min.y, col.bounds.max.y);
        zRandom = (float)Random.Range(col.bounds.min.z, col.bounds.max.z);

        // Debug.Log("Collider bound Minimum X: " + col.bounds.min.x);
        // Debug.Log("Collider bound Maximum X: " + col.bounds.max.x);
        // Debug.Log("Collider bound Minimum Y: " + col.bounds.min.y);
        // Debug.Log("Collider bound Maximum Y: " + col.bounds.max.y);
        // Debug.Log("Collider bound Minimum Z: " + col.bounds.min.z);
        // Debug.Log("Collider bound Maximum Z: " + col.bounds.max.z);

        return new Vector3(xRandom, yRandom, zRandom);
    }


    IEnumerator SpawnDestroyerTimer(int waitFor)
    {
        // audioData.Play();
        yield return new WaitForSeconds(waitFor);
        // spawnDestroyer = true;
        // Instantiate(explodingObject, transform.position, transform.rotation);
        // stallingSound.Stop();
        // Destroy(gameObject);
        // GenerateObject(DestroyObjectBall, numberOfDEstroyObectBalls);

        // GameManager.GMinstance.ballsLostRaw ++;
    } 

}
