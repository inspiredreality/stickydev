﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitter_LowPuff : MonoBehaviour
{
    // public GameObject resetLowHitter;
    public float m_Speed;

    private Material m_material;

    private bool isDissolving, movePos;
    //moveRot not necessary at this point in time
    float fade = 0f, fadePace = 0.17f;
    public int scaleInt;
    private Vector3 startLocation, m_direction, m_rotation, m_scale;
    
    // private Quaternion startQuaternion; 

    void Start()
    {
        fade = 0f;
        scaleInt = 0;
        transform.localScale = new Vector3(scaleInt, scaleInt, scaleInt);

        m_material = GetComponent<Renderer>().material;
        // startLocation = gameObject.transform.position;
        // m_rotation = gameObject.transform.rotation;
        //THIS COMMENT MEANS 2ND GREEN CAPSULE WORKS AS EXPECTED NUTIL RESPOAWN IS 90 GREEES TOO MUCH ON THE Z AXIS - LOOKS LIKE Y AXIS

        // startQuaternion = GetComponent<Quaternion>();
        //start up on Y
        m_direction = new Vector3(0.0f, 1.0f, 0.0f);
        Debug.Log("Puff started");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (scaleInt < 99)
        {
            scaleInt ++;
        }
        else
        {
            isDissolving = true;
            movePos = true;
        }

        //+ is starting to disappear
        if(isDissolving)
        {
            fade += (Time.deltaTime * fadePace);
            if(fade >= 1f)
            {
                // Instantiate(resetLowHitter, startLocation, new Quaternion (0,0,0,0));
                Destroy(gameObject);
            }
        }
        if (movePos)
        {
            transform.position += m_direction * Time.deltaTime * m_Speed;
        }
        m_material.SetFloat("_Fade", fade);
        transform.localScale = new Vector3(scaleInt, scaleInt, scaleInt);
    }

    // private void OnCollisionEnter(Collision collision)
    // {
    //     if(collision.collider.name.Contains("ToLow"))
    //     {
    //         isDissolving = true;
    //         movePos = true;           
    //     }
    // }

}
