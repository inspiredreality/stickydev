using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public GameObject LookAtPlayerFace;
    public float dist = 0;
    private Vector3 scaleChange;
    private bool grabbed = false;
    // Start is called before the first frame update
    void Start()
    {
        LookAtPlayerFace = GameObject.Find("AttractionPoint");
        // scaleChange = new Vector3(0f, 0f, dist);
    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(LookAtPlayerFace.transform.position, transform.position);

        if(transform.parent.GetComponent<OVRGrabbable>().isGrabbed)
        {
            grabbed = true;
        }

        if (dist > 10f)
        {
            Destroy(transform.parent.gameObject);
        }

        if(grabbed)
        {
            transform.LookAt(LookAtPlayerFace.transform);
            // dist = Vector3.Distance(LookAtPlayerFace.transform.position, transform.position);
            
            scaleChange = new Vector3(1f, 1f, 6 * dist);
            transform.localScale = scaleChange;
            // transform.localScale.z += scaleChange;
            // transform.localScale.z += dist;
        } 

    }
}
