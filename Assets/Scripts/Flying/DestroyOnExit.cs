using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnExit : MonoBehaviour
{
    public string container;
    void OnTriggerExit(Collider other)
    {
        if(other.name.Contains(container)){
            Destroy(gameObject);
        }
    }
}
