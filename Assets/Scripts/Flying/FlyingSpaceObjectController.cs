﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FlyingSpaceObjectController : MonoBehaviour
{
    Rigidbody m_Rigidbody;
    // public float m_Speed = 1f;

    GameObject AttractionPoint;
    // public AudioSource attractingSound;
    public GameObject explodingObject;
    public Vector3 m_direction;

    // public Vector3 velocityReading;
    private Color ballColor;
    private Material m_material;
    // public string testing = "none";

    public float smallThrowableAccel = 11, bigThrowableAccel = 25, scaleSpeed;
    private float throwableAccel, scaleFloat;
    private int scaleInt;


    // Start is called before the first frame update
    void Awake()
    // void Start()
    {
        // set size to 0
        scaleInt = 0;
        scaleFloat = 0;
        transform.localScale = new Vector3(scaleInt, scaleInt, scaleInt);

        // AttractionPoint = GameObject.FindGameObjectWithTag("AttractionPoint");
        m_direction = -Vector3.forward;
        if(transform.name.Contains("crossfire"))
        {
            // AttractionPoint = GameObject.FindGameObjectWithTag("AttractionPointCrossfire");
            m_direction = new Vector3(-1.0f, 0.0f, -1.0f);
        }

        if(transform.name.Contains("small"))
        {
            throwableAccel = smallThrowableAccel;
        }
        else if (transform.name.Contains("big")) 
        {
            throwableAccel = bigThrowableAccel;
        }
        // transform.LookAt(AttractionPoint.transform.position);

        ballColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        GetComponent<Renderer>().material.color = ballColor;
        // m_material.SetColor("_Color", ballColor);
        // greyColor = explodingObject.GetComponent<Renderer>().material.color;
        // explodingObject.GetComponent<Renderer>().material.color = ballColor;
        // throwableAccel = throwableAccel - Random.value;
        // testing = "onStart";
    }


    void FixedUpdate()
    {
        // testing = "preFixed";
        // if (scaleInt < 99)
        if (scaleFloat < 1)
        {
            scaleFloat += scaleSpeed * 0.001f;
            // scaleInt ++;
            // transform.localScale = new Vector3(scaleInt, scaleInt, scaleInt);
            transform.localScale = new Vector3(scaleFloat, scaleFloat, scaleFloat);
        }
        // else
        // {
            // transform.position += -Vector3.forward * Time.deltaTime * throwableAccel;
            transform.position += m_direction * Time.deltaTime * throwableAccel;
        // }


        // velocityReading = GetComponent<Rigidbody>().velocity;

        // testing = "beenFixed";
    }

    // void FixedUpdate()
    // {
    //     testing = "preFixed";

    //     transform.position += -Vector3.forward * Time.deltaTime * throwableAccel;
    //     velocityReading = GetComponent<Rigidbody>().velocity;

    //     testing = "beenFixed";
    // }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "DestroyObject" )
        {   
            //explode ball
            // rend.material.color = Color.HSVToRGB(9, 77, 77);
            // attractingSound.Stop();
            // Instantiate(explodingObject, transform.position, transform.rotation);
            // explodingObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            Instantiate(explodingObject, transform.position, transform.rotation);
            // explodingObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            Destroy(gameObject);
        }
        else if(other.gameObject.tag == "DisappearObject" )
        {   
            //make ball disappear
            Destroy(gameObject);
        }

    }

    // IEnumerator LoseTimerStarted()
    // {
    //     // audioData.Play();
    //     yield return new WaitForSeconds(2);
    //     // LoseCondition = true;
    // } 

}
