using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetParentRenderer : MonoBehaviour
{
    private Color ballColor;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ballColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);;
        // ballColor = transform.parent.GetComponent<Renderer>().material.color;
        GetComponent<Renderer>().material.color = ballColor;

    }
}
