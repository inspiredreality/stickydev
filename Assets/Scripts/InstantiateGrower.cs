﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateGrower : MonoBehaviour
{
    public GameObject smallBallMarker, bigBallMarker, smallCubeMarker, bigCubeMarker, 
    smallCylinderMarker, bigCylinderMarker, smallPyramidMarker, bigPyramidMarker;
    // public bool canInstantiate = true;
    //wont work for PlayerEnvironmentController bc we have multiple PlayerEnvironmentControllers, 1 on each ball
    // ARFGameController m_ARFGameController;
    // private Transform GrowerTransform;
    // private Rotation;
    // private Vector3 growPos, growRot;
    void Awake()
    {
        // m_ARFGameController = FindObjectOfType <ARFGameController>();
        
    }

    private void OnCollisionExit(Collision collision)
    {
        // if(collision.PlayerEnvironmentController.GetBallStatus() == "Attract")
        // if(collision.collider.ballStatus == "Attract" && canInstantiate)
        // if(collision.collider.tag == "Attract" && canInstantiate)
        if(collision.collider.tag == "Attract")
        {
            // canInstantiate = false;
            //can't set game object tag...
            // collision.collider.tag == "Attract";

            string thrownObjectName = collision.collider.name;
            // Vector3 growPos = collision.transform.position;
            // Vector3 growRot = collision.transform.rotation;
            // StartCoroutine(InstantiateGrower(thrownObjectName, growPos, growRot));


            if (thrownObjectName.Contains("big"))
            {
                if (thrownObjectName.Contains("Sphere"))
                {
                    var newMarker = Instantiate(bigBallMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cube"))
                {
                    var newMarker = Instantiate(bigCubeMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cylinder"))
                {
                    var newMarker = Instantiate(bigCylinderMarker, collision.transform.position, collision.transform.rotation);
                }            
                else if(thrownObjectName.Contains("Pyramid"))
                {
                    var newMarker = Instantiate(bigPyramidMarker, collision.transform.position, collision.transform.rotation);
                }            
            }
            else if(thrownObjectName.Contains("small"))
            {
                if (thrownObjectName.Contains("Sphere"))
                {
                    Debug.Log("instantiate small sphere");

                    var newMarker = Instantiate(smallBallMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cube"))
                {
                    var newMarker = Instantiate(smallCubeMarker, collision.transform.position, collision.transform.rotation);
                }
                else if(thrownObjectName.Contains("Cylinder"))
                {
                    var newMarker = Instantiate(smallCylinderMarker, collision.transform.position, collision.transform.rotation);
                }            
                else if(thrownObjectName.Contains("Pyramid"))
                {
                    var newMarker = Instantiate(smallPyramidMarker, collision.transform.position, collision.transform.rotation);
                }
            }

            // Debug.Log("OnCollisionExit Sticky  "  + collision.transform.tag);
            // Debug.Log("OnCollisionExit Sticky  " + collision.transform.name);
        
            
            // if(collision.collider.name == "bigBall1" || collision.collider.name == "bigBall2")
            // {
            //     var newMarker = Instantiate(bigMarker, collision.transform.position, collision.transform.rotation);
            //     // newMarker.gameObject.tag = "LoseCondition";
            //     Debug.Log("Big");
            // }
            // else
            // {
            //     var newMarker = Instantiate(smallMarker, collision.transform.position, collision.transform.rotation);
            //     // newMarker.gameObject.tag = "LoseCondition";
            //     Debug.Log("Small");
            // }

            // Debug.Log("StartPaused");

            // StartCoroutine(PauseMarkerInstantiation());
            // Debug.Log("EndPaused");

            // canInstantiate = true;
        }
    }

    // IEnumerator InstantiateGrower(string growName, Vector3 growPos, Vector3 growRot)
    // private InstantiateGrower(string growName, Vector3 growPos, Vector3 growRot)
    // {
    //     yield return new WaitForSeconds(.5f);
    //     Debug.Log("finished waiting .5 secs");

    //     if (growName.Contains("big"))
    //     {
    //         if (growName.Contains("Sphere"))
    //         {
    //             var newMarker = Instantiate(bigBallMarker, growPos, growRot);
    //         }
    //         else if(growName.Contains("Cube"))
    //         {
    //             var newMarker = Instantiate(bigCubeMarker, growPos, growRot);
    //         }
    //         else if(growName.Contains("Cylinder"))
    //         {
    //             var newMarker = Instantiate(bigCylinderMarker, growPos, growRot);
    //         }            
    //         else if(growName.Contains("Pyramid"))
    //         {
    //             var newMarker = Instantiate(bigPyramidMarker, growPos, growRot);
    //         }            
    //     }
    //     // else if(thrownObjectName.Contains("small"))
    //     // {
    //     //     if (thrownObjectName.Contains("Sphere"))
    //     //     {
    //     //         Debug.Log("instantiate small sphere");

    //     //         var newMarker = Instantiate(smallBallMarker, collision.transform.position, collision.transform.rotation);
    //     //     }
    //     //     else if(thrownObjectName.Contains("Cube"))
    //     //     {
    //     //         var newMarker = Instantiate(smallCubeMarker, collision.transform.position, collision.transform.rotation);
    //     //     }
    //     //     else if(thrownObjectName.Contains("Cylinder"))
    //     //     {
    //     //         var newMarker = Instantiate(smallCylinderMarker, collision.transform.position, collision.transform.rotation);
    //     //     }            
    //     //     else if(thrownObjectName.Contains("Pyramid"))
    //     //     {
    //     //         var newMarker = Instantiate(smallPyramidMarker, collision.transform.position, collision.transform.rotation);
    //     //     }
    //     // }
    // }
}
