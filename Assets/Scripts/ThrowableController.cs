using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableController : MonoBehaviour
{
    GameObject AttractionPoint;
    GameObject StopAttraction;
    public GameObject explodingObject, growingObject;
    public float gravitationalAcceleration = .0005f, smallThrowableAccel = 11, bigThrowableAccel = 8, timeCount;
    public Vector3 velocityReading;
    public AudioSource attractingSound, stallingSound, thrownSound, stuckSound;
    public string ballStatus = "Default";
    //Attract
    //Stall
    //Grabbed??
    //Thrown
    //StickyTimerStart
    public bool ballStatusColorChange = false;
    private float throwableAccel;
    public bool inFallZone = false;

    // stickySecStart is initial wait time, will be - with hitting StartSticky and + for doing good things
    Renderer rend;

    void Awake()
    {
        attractingSound = GameObject.Find("Attract").GetComponent<AudioSource>();
        stallingSound = GameObject.Find("Stall").GetComponent<AudioSource>();
        thrownSound = GameObject.Find("Thrown").GetComponent<AudioSource>(); 
        stuckSound = GameObject.Find("Scoring").GetComponent<AudioSource>();
    }
    void Start()
    {
    
        AttractionPoint = GameObject.FindGameObjectWithTag("AttractionPoint");
        // StopAttraction = GameObject.FindGameObjectWithTag("StopAttraction");
        ballStatus = "Attract";

        rend = GetComponent<Renderer>();

        if(transform.name.Contains("small"))
        {
            throwableAccel = smallThrowableAccel;
        }
        else if (transform.name.Contains("big")) 
        {
            throwableAccel = bigThrowableAccel;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // timeCount = Time.time;
        //if Win or Lose condition then stop sound
        if(GameManager.GMinstance.WinCondition || GameManager.GMinstance.LoseCondition)
        {
            attractingSound.Stop();
            stallingSound.Stop();
            thrownSound.Stop(); 
            stuckSound.Stop();
        }
    }

    public void FixedUpdate()
    {

        if(transform.GetComponent<OVRGrabbable>().isGrabbed)
        {
            ballStatus = "Grabbed";
        }

        switch (ballStatus)
        {
            case "Attract":
                Attracting();
                gameObject.tag = "Attract";
                // audioData.Play();
                break;
            case "Stall":
                Stalling();
                // gameObject.tag = "Stall";
                break;
            case "Grabbed":
                Grabbing();
                // gameObject.tag = "Grabbed";
                break;                            
            case "Thrown":
                Thrown();
                gameObject.tag = "Thrown";
                break;     
            case "Stuck":
                Stuck();
                // gameObject.tag = "Stuck";
                break;     
            default:
                break;                      
        }
    }

    private void Attracting()
    {
        //change so it doesnt pick up speed so quickly
        //make so every 10 or 5 secs ball picks up speed incrementally so attractionForce is a constant that increments rather than gravAccel multiplying 
        // GetComponent<Rigidbody>().velocity += gravitationalAcceleration * Time.fixedTime * (AttractionPoint.transform.position - transform.position);
        GetComponent<Rigidbody>().velocity += gravitationalAcceleration * throwableAccel * (AttractionPoint.transform.position - transform.position);
        // gameObject.transform.position = Vector3.Slerp(gameObject.transform.position, AttractionPoint.transform.position, Time.deltaTime * gravitationalAcceleration);
        velocityReading = GetComponent<Rigidbody>().velocity;
        //use mathf.Abs to set -5/5 max

        if(!attractingSound.isPlaying){
            attractingSound.Play();
        }

        if(ballStatusColorChange)
        {
            rend.material.color = Color.green;
        }
    }

    private void Stalling()
    {        
        attractingSound.Stop();
        
        if(!stallingSound.isPlaying){
            stallingSound.Play();
        }

        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        if(ballStatusColorChange)
        {
            rend.material.color = Color.blue;
        }

        if(inFallZone)
        {
        //find closest direction to FallZone
        //could also more easily pick a random X direction
        //LookAt Attraction Point then move foreward, but if it's above then we dont solve the problem
        //move that way until outside FallZone
            transform.position = transform.position + new Vector3 (.003f, 0, 0);
        }
        else{
        //continue falling down until hitting platform
            transform.position = transform.position - new Vector3 (0, .003f, 0);
        }

    }

    private void Grabbing()
    {
        //maybe dont need this due to its implementation in GrabBegin
        // GetComponent<Rigidbody>().isKinematic = false;
        if(ballStatusColorChange)
        {
            rend.material.color = Color.red;
        }

        if(!transform.GetComponent<OVRGrabbable>().isGrabbed)
        {
            GetComponent<Rigidbody>().isKinematic = false;

            ballStatus = "Thrown";
        }

    }

    private void Thrown()
    {
        if(!thrownSound.isPlaying){
            thrownSound.Play();
        }
        

        //add tracers or something cool
        //stop
        // Debug.Log("Thrown");
        if(ballStatusColorChange)
        {
            rend.material.color = Color.yellow;
        }

        GetComponent<Rigidbody>().isKinematic = false;

    }

    private void Stuck()
    {
        //add pulsating and growing 
        //start line merge nif 2 balls stuck
        // ballStatus = "Attract";
        thrownSound.Stop();
        if(!stuckSound.isPlaying){
            stuckSound.Play();
        }
        if(ballStatusColorChange)
        {
            rend.material.color = Color.black;
        }
        //Scoring
        Scoring();    
    }

    private void Scoring()
    {
        if (transform.name.Contains("big"))
        {
            if (transform.name.Contains("Sphere"))
            {
                GameManager.GMinstance.bigBallScore += Time.deltaTime;
            }
            else if(transform.name.Contains("Cube"))
            {
                GameManager.GMinstance.bigCubeScore += Time.deltaTime;
            }
            else if(transform.name.Contains("Cylinder"))
            {
                GameManager.GMinstance.bigCylinderScore += Time.deltaTime;
            }            
            else if(transform.name.Contains("Pyramid"))
            {
                GameManager.GMinstance.bigPyramidScore += Time.deltaTime;
            }            
        }
        else if(transform.name.Contains("small"))
        {
            if (transform.name.Contains("Sphere"))
            {
                GameManager.GMinstance.smallBallScore += Time.deltaTime;
            }
            else if(transform.name.Contains("Cube"))
            {
                GameManager.GMinstance.smallCubeScore += Time.deltaTime;
            }
            else if(transform.name.Contains("Cylinder"))
            {
                GameManager.GMinstance.smallCylinderScore += Time.deltaTime;
            }            
            else if(transform.name.Contains("Pyramid"))
            {
                GameManager.GMinstance.smallPyramidScore += Time.deltaTime;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //check if StopAttraction collider rather than other possible colliders in the env
        if(ballStatus == "Attract" && other.gameObject.name == "StopAttraction")
        {
            inFallZone = true;
            // Debug.Log("In Fall Zone");
            ballStatus = "Stall";
            GetComponent<Rigidbody>().isKinematic = true;
        }

        if(ballStatus == "Stall" && other.gameObject.name == "PlatformTrigger" )
        {
            //explode ball
            rend.material.color = Color.HSVToRGB(9, 77, 77);
            ballStatus = "Default";
            StartCoroutine(ExplodeTimerStarted());
        }
    }

    IEnumerator ExplodeTimerStarted()
    {
        // audioData.Play();
        yield return new WaitForSeconds(2);
        Instantiate(explodingObject, transform.position, transform.rotation);
        stallingSound.Stop();
        Destroy(gameObject);

        GameManager.GMinstance.ballsLostRaw ++;
    } 

    private void OnTriggerStay(Collider other)
    {
        //check if outside FallZone to start falling down instead of sideways
        if(ballStatus == "Stall" && other.gameObject.name == "FallZone")
        {
            inFallZone = true;
            // Debug.Log("Inside Fall Zone");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //check if outside FallZone to start falling down instead of sideways
        if(ballStatus == "Stall" && other.gameObject.name == "FallZone")
        {
            inFallZone = false;
            // Debug.Log("Exit Fall Zone");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log("OnCollisionEntered-PEC- " + transform.name.ToString() + " collided " + collision.transform.name);

        // if (collision.tag == "StartSticky" && ballStatus == "Thrown")
        if(ballStatus == "Thrown" && collision.collider.tag == "StartSticky")
        {
            // if(collision.collider.tag == "StartSticky" )
            // {
                ballStatus = "Stuck";
                // gameObject.tag = "Grower";
                
                GetComponent<Rigidbody>().isKinematic = true;
                StartCoroutine(StickyTimerStarted());
                
            // }

            //another like StaretSticky not just bounce back
            //maybe this better in a separate script to attach to balls/objects that dont stick
        }
    }
    IEnumerator StickyTimerStarted()
    {
        if (transform.name.Contains("big"))
        {
            yield return new WaitForSeconds(GameManager.GMinstance.stickySecStartBig);
        }
        else
        {
            yield return new WaitForSeconds(GameManager.GMinstance.stickySecStartSmall);
        }
        // yield return new WaitForSeconds(3);
        stuckSound.Stop();
        // ballStatus = "Attract";
        //add Grown tag for 1 cycle before tag= Attract
        // gameObject.tag = "Grower";
        ballStatus = "Attract";

        GetComponent<Rigidbody>().isKinematic = false;

        //set .5s timer to instantiate grower
        StartCoroutine(InstantiateGrower(transform.position, transform.rotation));
    } 

    IEnumerator InstantiateGrower(Vector3 growPos, Quaternion growRot)
    {
        yield return new WaitForSeconds(.5f);
        Instantiate(growingObject, growPos, growRot);
    } 
}

