﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectTimer : MonoBehaviour
{
    public int delayDestroySecs = 3;
    // float fade = 0f, fadePace = 0.17f;
    private Material m_material;

    void Awake()
    {
        // m_material = GetComponent<Renderer>().material;
    }
    void FixedUpdate()
    {
        // fade += (Time.deltaTime * fadePace);
        StartCoroutine(DestroyerTimer());
        // m_material.SetFloat("_Fade", fade);
    }
    IEnumerator DestroyerTimer()
    {
        // audioData.Play();
        yield return new WaitForSeconds(delayDestroySecs);
        Destroy(gameObject);
    }
}
