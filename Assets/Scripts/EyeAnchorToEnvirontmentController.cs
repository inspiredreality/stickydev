﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeAnchorToEnvirontmentController : MonoBehaviour
{
    public float fallSpeed = .0001f;
    public bool playerFalling = false;
    public GameObject trackingSpace;
    // public GameObject OVRCameraRigParent;

    // public string playerStatus = "Default";

    // private void OnTriggerEnter(Collider other)
    private void OnTriggerExit(Collider other)
    {
        // Debug.Log("OnTriggerExit-EATEC- " + transform.name.ToString() + " triggered name: " + other.transform.name);
        // Debug.Log("OnTriggerExit-EATEC- " + transform.name.ToString() + " triggered tag: " + other.transform.tag);

        if(other.gameObject.name == "FallZone")
        {
            // ballStatus = "Stall";
            // GetComponent<Rigidbody>().isKinematic = true;
            playerFalling = true;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("Trigger Enter EATEC- " + transform.name.ToString() + " collided with tag: " + other.gameObject.tag);

        if(other.gameObject.tag == "LoseCondition")
        {
            GameManager.GMinstance.LoseCondition = true;
            // GameManager.GMinstance.pauseGame();
        }
        // if(other.gameObject.tag == "LoseCondition")
        // {
        //     GameManager.GMinstance.LoseCondition = true;
        //     GameManager.GMinstance.pauseGame();
        // }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log("OnTriggerEntered-EATEC- " + transform.name.ToString() + " triggered name: " + other.transform.name);
        // Debug.Log("OnTriggerEntered-EATEC- " + transform.name.ToString() + " triggered tag: " + other.transform.tag);
        // Debug.Log("Collision Enter EATEC- " + transform.name.ToString() + " collided with tag: " + collision.transform.tag);

        if(collision.transform.tag == "StartSticky")
        // if(other.gameObject.name == "half-shell-right" || other.gameObject.name == "half-shell-left" )
        {
            // ballStatus = "Stall";
            playerFalling = false;
            Debug.Log("EATEC hit bottom");

        }
    
        if(collision.transform.tag == "LoseCondition")
        {
            GameManager.GMinstance.LoseCondition = true;
            // GameManager.GMinstance.pauseGame();
            Debug.Log("EATEC Game Over");

        }
    
    }

    public void FixedUpdate()
    {
        
        if(playerFalling)
        {
            // transform.position = transform.position - new Vector3 (0, fallSpeed, 0);
            trackingSpace.transform.position = transform.position - new Vector3 (0, fallSpeed, 0);
            
        }

        // switch (ballStatus)
        // {
        //     case "Falling":
        //         Falling();
        //         break;
        //     case "Stall":
        //         Stalling();
        //         break; 
        //     default:
        //         break;                      
        // }


    }

    // private void Falling()
    // {
    //     transform.position = transform.position - new Vector3 (0, fallSpeed, 0);
    // }
}
