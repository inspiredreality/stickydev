﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowMarker : MonoBehaviour
{
    public float scaleRate;

    void Update()
    {
        if(!GameManager.GMinstance.paused && !GameManager.GMinstance.WinCondition && !GameManager.GMinstance.LoseCondition)
        {
            // gameObject.transform.localScale = gameObject.transform.localScale + new Vector3(gameObject.transform.localScale.x*scaleRate, gameObject.transform.localScale.y*scaleRate,gameObject.transform.localScale.z*scaleRate,);

            //issues with inconsistent scale from blender import (1+scale rate is good speed but 100 + .0025 scale rate is nothing)
            // gameObject.transform.localScale = gameObject.transform.localScale + new Vector3((float)scaleRate, (float)scaleRate, (float)scaleRate);

            gameObject.transform.localScale = gameObject.transform.localScale + new Vector3((float)scaleRate, (float)scaleRate, (float)scaleRate);
        
        }

    }
}
