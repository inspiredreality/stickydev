using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectLevel : MonoBehaviour
{
    public int idx;
    public float waitSecs;
    public GameObject highObj, lowObj;
    public bool highObjGravity = false, lowObjGravity = false;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if(other.name.Contains("Destroyer")){
            if(highObjGravity) {
                highObj.GetComponent<Rigidbody>().useGravity = true;
            }
            if(lowObjGravity) {
                lowObj.GetComponent<Rigidbody>().useGravity = true;
            }
            highObj.GetComponent<Rigidbody>().isKinematic = false;
            lowObj.GetComponent<Rigidbody>().isKinematic = false;
            StartCoroutine(LvlSelectedTimer());
        }
    }

    IEnumerator LvlSelectedTimer()
    {
        yield return new WaitForSeconds(waitSecs);
        UnityEngine.SceneManagement.SceneManager.LoadScene(idx);
    }

}
