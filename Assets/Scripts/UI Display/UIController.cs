﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class UIController : MonoBehaviour
{
    public static UIController UIinstance;

    public bool showMenu = true, canExecute=true, staticUICanvas = false;
    private Text sliderText;
    private Vector3 placedTrans, placedRot;
    RectTransform lblTotalScore, lblBallsLost, lblRealTime, lblLeftPanel, lblLeftPanelLeaders, lblWinner,
    lblPlayerPlaying;
    GameObject btnTryAgain, btnNextLevel, btnPause, lblRight, lblLeft, lblRightParagraph, btnBrian, 
    btnLeah, btnEric, btnDavid, btnJess, btnGreg, btnGuest;

    void Awake()
    {
        // if (UIinstance == null)
        // {
        //     //assign it to current object
        //     UIinstance = this;
        // }
        // //make sure it is the current object
        // else if (UIinstance != this)
        // {
        //     //destroy game object, we only need 1 and it already exists
        //     Destroy(gameObject);
        // }
        // // DontDestroyOnLoad(gameObject);

    }
    // Start is called before the first frame update
    void Start()
    {
        Scene scene = SceneManager.GetActiveScene();
        //Set Level name and indexes
        if (GameObject.Find("StickyGameManager") != null){
            GameManager.GMinstance.thisLevelName = scene.name;
            GameManager.GMinstance.thisLevelIndex = scene.buildIndex;
        }

        // if(scene.name == "StartFromLvl1" || scene.name == "StickyStartScene" || 
        // scene.name == "StickyStartScene 2" || scene.name == "SelectPlayer" || scene.name == "StickyStartScene 3" ){
        if(scene.name.Contains("StickyStart")){
            staticUICanvas = true;
            buildStartMenu();
        }
        // else if(scene.name.Contains())
        // {
        //     staticUICanvas = false;
        //     buildLevelMenu();
        // }
        else 
        {
            staticUICanvas = false;
            buildLevelMenu();
        }
        StickyUIBuilder.instance.StartShow();
    }

    void buildStartMenu()
    {
        //Show Player
        lblPlayerPlaying = StickyUIBuilder.instance.AddLabel("Playing Player: " + LeaderboardTracking.LTinstance.playingPlayer);
                
        StickyUIBuilder.instance.AddLabel("Select Level");
        
        int n = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
        //i = 1 instead of 0 to not show SSS as an option to choose, High Score is first option as #1 scene
        for (int i = 1; i < n; ++i)
        {
            string path = UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i);
            var sceneIndex = i;
            StickyUIBuilder.instance.AddButton(Path.GetFileNameWithoutExtension(path), () => LoadScene(sceneIndex));
        }

        // //Left Panel Leaderboard
        lblLeftPanelLeaders = StickyUIBuilder.instance.AddLabel("!!Leaderboard!!",2);
        StickyUIBuilder.instance.AddDivider(2);
        lblLeftPanel = StickyUIBuilder.instance.AddLabel("Select Player",2);
        // StickyUIBuilder.instance.AddDivider(2);
        btnBrian = StickyUIBuilder.instance.AddButton("Jack  ...  High Score: " + LeaderboardTracking.LTinstance.brianScore + ", Level: " + LeaderboardTracking.LTinstance.rawBrianLevel.ToString(), brianPressed, 2).gameObject;
        btnLeah = StickyUIBuilder.instance.AddButton("Casie  ...  High Score: " + LeaderboardTracking.LTinstance.leahScore + ", Level: " + LeaderboardTracking.LTinstance.rawLeahLevel.ToString(), leahPressed, 2).gameObject;
        btnEric = StickyUIBuilder.instance.AddButton("Andrew  ...  High Score: " + LeaderboardTracking.LTinstance.ericScore + ", Level: " + LeaderboardTracking.LTinstance.rawEricLevel.ToString(), ericPressed, 2).gameObject;
        btnDavid = StickyUIBuilder.instance.AddButton("David  ... High Score: " + LeaderboardTracking.LTinstance.davidScore + ", Level: " + LeaderboardTracking.LTinstance.rawDavidLevel.ToString(), davidPressed, 2).gameObject;
        btnJess = StickyUIBuilder.instance.AddButton("Rach  ... High Score: " + LeaderboardTracking.LTinstance.jessScore + ", Level: " + LeaderboardTracking.LTinstance.rawJessLevel.ToString(), jessPressed, 2).gameObject;
        // btnGreg = StickyUIBuilder.instance.AddButton("Greg  ... High Score: " + LeaderboardTracking.LTinstance.gregScore + ", Level: " + LeaderboardTracking.LTinstance.rawGregLevel.ToString(), gregPressed, 2).gameObject;
        // btnGuest = StickyUIBuilder.instance.AddButton("Guest  ... High Score: " + LeaderboardTracking.LTinstance.guestScore + ", Level: " + LeaderboardTracking.LTinstance.rawGuestLevel.ToString(), guestPressed, 2).gameObject;

        StickyUIBuilder.instance.StartShow();
    }

//load scene for Start Menu
    void LoadScene(int idx)
    {
        StickyUIBuilder.instance.Hide();
        Debug.Log("Load scene: " + idx);
        // UnityEngine.SceneManagement.SceneManager.LoadScene(idx);
        UnityEngine.SceneManagement.SceneManager.LoadScene(idx);
    }



    void buildLevelMenu(){
        // StickyUIBuilder.instance.AddLabel("Sticky Balls Menu");
        // string path = UnityEngine.SceneManagement.SceneUtility.GetActiveScene();
        // UnityEngine.SceneManagement.GetFileNameWithoutExtension(scene);
        StickyUIBuilder.instance.AddLabel(GameManager.GMinstance.thisLevelName.ToString());

        StickyUIBuilder.instance.AddDivider();
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("STICKY SCORE");
        lblTotalScore = StickyUIBuilder.instance.AddLabel("0");
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("Balls Lost");
        lblBallsLost = StickyUIBuilder.instance.AddLabel("0");
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("Real Time");
        lblRealTime = StickyUIBuilder.instance.AddLabel("0");
        // StickyUIBuilder.instance.AddDivider();

        // //Left Panel Lose
        lblLeftPanel = StickyUIBuilder.instance.AddLabel("Pause or Restart",2);
        StickyUIBuilder.instance.AddDivider(2);
        btnPause = StickyUIBuilder.instance.AddButton("Pause Game", PauseButtonPressed, 2).gameObject;
        btnTryAgain = StickyUIBuilder.instance.AddButton("Restart Level", tryAgainPressed, 2).gameObject;
        // btnTryAgain.SetActive(false);

        // //Right Panel Win
        lblWinner = StickyUIBuilder.instance.AddLabel("How to Win", 1);
        StickyUIBuilder.instance.AddDivider(1);
        lblRightParagraph = StickyUIBuilder.instance.AddLabel("Sticking balls score while stuck \n -10 points for each lost ball \n Lose if blackness engulfs you", 1).gameObject;
        btnNextLevel = StickyUIBuilder.instance.AddButton("Sticky Start Menu", NextLevelPressed, 1).gameObject;
        // btnNextLevel.SetActive(false);

        // StickyUIBuilder.instance.StartShow();        
    }

    void Update()
    {

        if(OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.Three))
        {
            showMenu = !showMenu;
        }
        if (showMenu){
            StickyUIBuilder.instance.Show();
        }
        else{
            StickyUIBuilder.instance.Hide();
        }
        
        //for non main menu levels, all of which will have a GMinstance
        if(!staticUICanvas)
        {
            //if not in title scene then manage StickyGameManager stuff
            // if (showMenu){
            //     StickyUIBuilder.instance.Show();
            // }
            // else{
            //     StickyUIBuilder.instance.Hide();
            // }

            // if(OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.Three))
            // {
            //     // if (showMenu) StickyUIBuilder.instance.Hide();
            //     // // if (showMenu) StickyUIBuilder.instance.SetActive(false);
            //     // else StickyUIBuilder.instance.Show();


            //     // else StickyUIBuilder.instance.Show();
            //     // else StickyUIBuilder.instance.SetActive(true);

            //     //instantiate new one instead of hide/unhide
            //     // if (showMenu) StickyUIBuilder.instance.Hide();
            //     // else 
            //     // {
            //     //     placedTrans = transform.TransformPoint(0,0,0);
            //     //     // GameObject here = new GameObject();
            //     //     // placedTrans = transform.position;
            //     //     // here.transform.position = gameObect.transform.position;
            //     //     // menuShown.transform.position = placedTrans;
            //     // }
            //     showMenu = !showMenu;
            // }
            lblTotalScore.GetComponent<Text>().text = GameManager.GMinstance.totalScore;
            lblBallsLost.GetComponent<Text>().text = GameManager.GMinstance.ballsLost;
            lblRealTime.GetComponent<Text>().text = GameManager.GMinstance.realTime;

            if(GameManager.GMinstance.LoseCondition && canExecute)
            {
                // btnTryAgain.SetActive(true);
                lblLeftPanel.GetComponent<Text>().text = "LOSER";
                ((Text)(btnTryAgain.GetComponentsInChildren(typeof(Text), true)[0])).text = "Try Again";
                // StickyUIBuilder.instance.AddLabel("LOSER",2);
                btnPause.SetActive(false);
                canExecute = false;
                ((Text)(btnNextLevel.GetComponentsInChildren(typeof(Text), true)[0])).text = "Sticky Start Menu";
            }
            if(GameManager.GMinstance.WinCondition && canExecute)
            {
                lblWinner.GetComponent<Text>().text = "WINNER";
                lblRightParagraph.SetActive(false);
                btnNextLevel.SetActive(true);
                canExecute = false;
                ((Text)(btnNextLevel.GetComponentsInChildren(typeof(Text), true)[0])).text = "Next Level";
            }
        }
        //for static UI aka start scene
        else
        {
            lblPlayerPlaying.GetComponent<Text>().text = "Playing Player: " + LeaderboardTracking.LTinstance.playingPlayer;

        }
    }

    void PauseButtonPressed()
    {
        if(GameManager.GMinstance.paused)
        {
            // btnPause.GetComponent<Text>().text = "Pause Game";
            ((Text)(btnPause.GetComponentsInChildren(typeof(Text), true)[0])).text = "Pause Game";
            GameManager.GMinstance.pauseGame();
        }
        else 
        {
            //playing
            // btnPause.GetComponent<Text>().text = "Resume Game";
            ((Text)(btnPause.GetComponentsInChildren(typeof(Text), true)[0])).text = "Resume Game";
            GameManager.GMinstance.pauseGame();
        }
    }

    void tryAgainPressed()
    {
        GameManager.GMinstance.clearScores();
        //set Pause in StickYGameManager
        // SceneManager.LoadScene(GameManager.GMinstance.thisLevelName, LoadSceneMode.Single);
        SceneManager.LoadSceneAsync(GameManager.GMinstance.thisLevelName.ToString(), LoadSceneMode.Single);
    }
    void NextLevelPressed()
    {
        //set Pause in StickYGameManager
        GameManager.GMinstance.clearScores();
        // SceneManager.LoadScene("FloatingBallsStickers");

        if(GameManager.GMinstance.WinCondition)
        {
            SceneManager.LoadScene(GameManager.GMinstance.thisLevelIndex + 1);
        }
        else 
        {
            SceneManager.LoadScene(0);
        }
        // UnityEngine.SceneManagement.SceneManager.LoadScene(GameManager.GMinstance.nextLevelIndex, LoadSceneMode.Single);
    }

    void brianPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Jack";
    }
    void leahPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Casie";
    }
    void ericPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Andrew";
    }
    void jessPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Rach";
    }
    void davidPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "David";
    }
    void gregPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Greg";
    }
    void guestPressed()
    {
        LeaderboardTracking.LTinstance.playingPlayer = "Guest";
    }

}
