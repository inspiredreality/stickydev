﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StickyUIMenu : MonoBehaviour
{
    public bool inMenu = false, canExecute=true;
    private Text sliderText;
    private Vector3 placedTrans, placedRot;
    RectTransform lblTotalScore, lblBallsLost, lblRealTime, lblLoser, lblWinner;
    // , btnTryAgain, btnNextLevel, btnPause;
    GameObject btnTryAgain, btnNextLevel, btnPause, lblRight, lblLeft, lblRightParagraph;

	void Start ()
    {
        //Set Level name and indexes
        Scene scene = SceneManager.GetActiveScene();
        GameManager.GMinstance.thisLevelName = scene.name;
        GameManager.GMinstance.thisLevelIndex = scene.buildIndex;

        // StickyUIBuilder.instance.AddLabel("Sticky Balls Menu");
        // string path = UnityEngine.SceneManagement.SceneUtility.GetActiveScene();
        // UnityEngine.SceneManagement.GetFileNameWithoutExtension(scene);
        StickyUIBuilder.instance.AddLabel(GameManager.GMinstance.thisLevelName.ToString());

        StickyUIBuilder.instance.AddDivider();
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("STICKY SCORE");
        lblTotalScore = StickyUIBuilder.instance.AddLabel("0");
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("Balls Lost");
        lblBallsLost = StickyUIBuilder.instance.AddLabel("0");
        StickyUIBuilder.instance.AddDivider();

        StickyUIBuilder.instance.AddLabel("Real Time");
        lblRealTime = StickyUIBuilder.instance.AddLabel("0");
        // StickyUIBuilder.instance.AddDivider();

        // //Left Panel Lose
        lblLoser = StickyUIBuilder.instance.AddLabel("Pause or Restart",2);
        StickyUIBuilder.instance.AddDivider(2);
        btnPause = StickyUIBuilder.instance.AddButton("Pause Game", PauseButtonPressed, 2).gameObject;
        btnTryAgain = StickyUIBuilder.instance.AddButton("Restart Level", tryAgainPressed, 2).gameObject;
        // btnTryAgain.SetActive(false);

        // //Right Panel Win
        lblWinner = StickyUIBuilder.instance.AddLabel("How to Win", 1);
        StickyUIBuilder.instance.AddDivider(1);
        lblRightParagraph = StickyUIBuilder.instance.AddLabel("Sticking balls score while stuck \n -10 points for each lost ball \n Lose if blackness engulfs you", 1).gameObject;
        btnNextLevel = StickyUIBuilder.instance.AddButton("Next Level", NextLevelPressed, 1).gameObject;
        // btnNextLevel.SetActive(false);

        StickyUIBuilder.instance.StartShow();
        // inMenu = true;
        // inMenu = false;
        // StickyUIBuilder.instance.Hide();
	}

    // public void TogglePressed(Toggle t)
    // {
    //     Debug.Log("Toggle pressed. Is on? "+t.isOn);
    // }
    // public void RadioPressed(string radioLabel, string group, Toggle t)
    // {
    //     Debug.Log("Radio value changed: "+radioLabel+", from group "+group+". New value: "+t.isOn);
    // }

    // public void SliderPressed(float f)
    // {
    //     Debug.Log("Slider: " + f);
    //     sliderText.text = f.ToString();
    // }

    void Update()
    {
        if (inMenu){
            StickyUIBuilder.instance.Show();
        }
        else{
            StickyUIBuilder.instance.Hide();
        }

        if(OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.Three))
        {
            // if (inMenu) StickyUIBuilder.instance.Hide();
            // // if (inMenu) StickyUIBuilder.instance.SetActive(false);
            // else StickyUIBuilder.instance.Show();


            // else StickyUIBuilder.instance.Show();
            // else StickyUIBuilder.instance.SetActive(true);

            //instantiate new one instead of hide/unhide
            // if (inMenu) StickyUIBuilder.instance.Hide();
            // else 
            // {
            //     placedTrans = transform.TransformPoint(0,0,0);
            //     // GameObject here = new GameObject();
            //     // placedTrans = transform.position;
            //     // here.transform.position = gameObect.transform.position;
            //     // menuShown.transform.position = placedTrans;
            // }
            inMenu = !inMenu;
        }
        lblTotalScore.GetComponent<Text>().text = GameManager.GMinstance.totalScore;
        lblBallsLost.GetComponent<Text>().text = GameManager.GMinstance.ballsLost;
        lblRealTime.GetComponent<Text>().text = GameManager.GMinstance.realTime;

        if(GameManager.GMinstance.LoseCondition && canExecute)
        {
            // btnTryAgain.SetActive(true);
            lblLoser.GetComponent<Text>().text = "LOSER";
            ((Text)(btnTryAgain.GetComponentsInChildren(typeof(Text), true)[0])).text = "Try Again";
            // StickyUIBuilder.instance.AddLabel("LOSER",2);
            btnPause.SetActive(false);
            canExecute = false;
        }
        if(GameManager.GMinstance.WinCondition && canExecute)
        {
            lblWinner.GetComponent<Text>().text = "WINNER";
            lblRightParagraph.SetActive(false);
            btnNextLevel.SetActive(true);
            canExecute = false;

        }

    }

    void PauseButtonPressed()
    {
        if(GameManager.GMinstance.paused)
        {
            // btnPause.GetComponent<Text>().text = "Pause Game";
            ((Text)(btnPause.GetComponentsInChildren(typeof(Text), true)[0])).text = "Pause Game";
            GameManager.GMinstance.pauseGame();
        }
        else 
        {
            //playing
            // btnPause.GetComponent<Text>().text = "Resume Game";
            ((Text)(btnPause.GetComponentsInChildren(typeof(Text), true)[0])).text = "Resume Game";
            GameManager.GMinstance.pauseGame();
        }
    }

    void tryAgainPressed()
    {
        GameManager.GMinstance.clearScores();
        //set Pause in StickYGameManager
        // SceneManager.LoadScene(GameManager.GMinstance.thisLevelName, LoadSceneMode.Single);
        SceneManager.LoadSceneAsync(GameManager.GMinstance.thisLevelName.ToString(), LoadSceneMode.Single);
    }
    void NextLevelPressed()
    {
        //set Pause in StickYGameManager
        GameManager.GMinstance.clearScores();
        // SceneManager.LoadScene("FloatingBallsStickers");
        SceneManager.LoadScene(0);
        // UnityEngine.SceneManagement.SceneManager.LoadScene(GameManager.GMinstance.nextLevelIndex, LoadSceneMode.Single);
    }
    //if playing then show pause button menu
    //if not playing then show pause button menu
}
