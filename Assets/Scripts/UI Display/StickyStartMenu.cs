﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickyStartMenu : MonoBehaviour
{
    // public OVROverlay overlay;
    // public OVROverlay text;
    // public OVRCameraRig vrRig;

    void Start()
    {
        StickyUIBuilder.instance.AddLabel("Select Level");
        
        int n = UnityEngine.SceneManagement.SceneManager.sceneCountInBuildSettings;
        for (int i = 0; i < n; ++i)
        {
            string path = UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i);
            var sceneIndex = i;
            StickyUIBuilder.instance.AddButton(Path.GetFileNameWithoutExtension(path), () => LoadScene(sceneIndex));
        }
        
        StickyUIBuilder.instance.StartShow();
    }

    void LoadScene(int idx)
    {
        StickyUIBuilder.instance.Hide();
        Debug.Log("Load scene: " + idx);
        // UnityEngine.SceneManagement.SceneManager.LoadScene(idx);
        UnityEngine.SceneManagement.SceneManager.LoadScene(idx);
    }
}
