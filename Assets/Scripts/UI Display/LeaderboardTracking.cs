using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardTracking : MonoBehaviour
{
    public static LeaderboardTracking LTinstance;

    public string playingPlayer = "Jack", brianScore="0", leahScore="0", ericScore="0", davidScore="0", jessScore="0", gregScore="0", guestScore="0";
    // ,brianScore="0", leahScore="0", ericScore="0", davidScore="0", jessScore="0", gregScore="0", guestScore="0";

    public int rawBrianScore = 0, rawLeahScore=0, rawEricScore=0, rawDavidScore=0, rawJessScore=0, rawGregScore=0, rawGuestScore=0,
    rawBrianLevel = 0, rawLeahLevel=0, rawEricLevel=0, rawDavidLevel=0, rawJessLevel=0, rawGregLevel=0, rawGuestLevel=0;

    //track level for Fastest Time
    // public float lvl1BestTime;
    // public string lvl1BestPlayer, SSSScoreboardStats;
    // //SSSScoreboardStats is JSON to serialize in LBT and deserialize in SSSS
    // private int thisLevelIndex;

    //key value array to track Level#, Best Time, Best Time Holder
     

    void Awake()
    {
        if (LTinstance == null)
        {
            //assign it to current object
            LTinstance = this;
        }
        //make sure it is the current object
        else if (LTinstance != this)
        {
            //destroy game object, we only need 1 and it already exists
            Destroy(gameObject);
        }
        // //keep alive, dont destroy when scene changes
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setPlayerHighScore(int scored)
    {
        switch (playingPlayer)
        {
            case "Jack":
                if(scored > rawBrianScore)
                {
                    brianScore = scored.ToString();
                    rawBrianScore = scored;
                }
                // brianScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Casie":
                if(scored > rawLeahScore)
                {
                    leahScore = scored.ToString();
                    rawLeahScore = scored;
                }
                // leahScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Andrew":
                if(scored > rawEricScore)
                {
                    ericScore = scored.ToString();
                    rawEricScore = scored;
                }
                // ericScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "David":
                if(scored > rawDavidScore)
                {
                    davidScore = scored.ToString();
                    rawDavidScore = scored;
                }
                // davidScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Rach":
                if(scored > rawJessScore)
                {
                    jessScore = scored.ToString();
                    rawJessScore = scored;
                }
                // jessScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Greg":
                if(scored > rawGregScore)
                {
                    gregScore = scored.ToString();
                    rawGregScore = scored;
                }
                // gregScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Guest":
                if(scored > rawGuestScore)
                {
                    guestScore = scored.ToString();
                    rawGregScore = scored;
                }
                // guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;        
            default:
                // guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
        }
    }

    public void setPlayerFurthestLevel(int levelCompleted)
    {
        switch (playingPlayer)
        {
            case "Brian":
                if(levelCompleted > rawBrianLevel)
                {
                    rawBrianLevel = levelCompleted;
                }
                // brianScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Leah":
                if(levelCompleted > rawLeahScore)
                {
                    rawLeahLevel = levelCompleted;
                }
                // leahScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Eric":
                if(levelCompleted > rawEricScore)
                {
                    rawEricLevel = levelCompleted;
                }
                // ericScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "David":
                if(levelCompleted > rawDavidScore)
                {
                    rawDavidLevel = levelCompleted;
                }
                // davidScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Jess":
                if(levelCompleted > rawJessScore)
                {
                    rawJessLevel = levelCompleted;
                }
                // jessScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Greg":
                if(levelCompleted > rawGregScore)
                {
                    rawGregLevel = levelCompleted;
                }
                // gregScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
            case "Guest":
                if(levelCompleted > rawGuestScore)
                {
                    rawGregLevel = levelCompleted;
                }
                // guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;        
            default:
                // guestScore = Mathf.FloorToInt(totalScoreRaw).ToString();
                break;
        }
    }


    //determine if best time is beaten
    // JsonUtility.FromJsonOverwrite(json, myObject);

}
