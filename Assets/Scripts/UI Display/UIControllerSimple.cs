using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;


public class UIControllerSimple : MonoBehaviour
{

    GameObject btnStartMenu;
    public bool showMenu = false;
    // Start is called before the first frame update
    void Start()
    {
        StickyUIBuilder.instance.AddLabel("Explore the Cubes");
        btnStartMenu = StickyUIBuilder.instance.AddButton("Sticky Start Menu", btnStartMenuPresseed).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(OVRInput.GetDown(OVRInput.Button.One) || OVRInput.GetDown(OVRInput.Button.Three))
        {
            showMenu = !showMenu;
        }
        if (showMenu){
            StickyUIBuilder.instance.Show();
        }
        else{
            StickyUIBuilder.instance.Hide();
        }   
    }

    void btnStartMenuPresseed()
    {
        SceneManager.LoadScene(0);
    }
}
