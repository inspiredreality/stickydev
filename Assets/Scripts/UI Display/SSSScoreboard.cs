using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SSSScoreboard : MonoBehaviour
{
    public TextMeshPro txtLvl1FastestTimeName, txtLvl1FastestTimeVal, 
    txtLvl2FastestTimeName, txtLvl2FastestTimeVal, 
    txtLvlHSFastestTimeName, txtLvlHSFastestTimeVal;

    //use Start to un-serialize JSON from LBT
    void Start()
    {
        
    }


    void Update()
    {
        // (TextMeshPro)
        // txtLvl1FastestTimeName.text = LeaderboardTracking.LTinstance.playingPlayer + "'s Score";
        // txtLvl1FastestTimeVal.text = GameManager.GMinstance.totalScore;

        // txtLvl2FastestTimeName.text = "-" + (GameManager.GMinstance.ballsLostRaw*10).ToString();
        // txtLvl2FastestTimeVal.text = GameManager.GMinstance.realTime;


        txtLvl1FastestTimeName.text = "David";
        txtLvl1FastestTimeVal.text = "01:03";

        txtLvl2FastestTimeName.text = "Rach";
        txtLvl2FastestTimeVal.text = "00:32";

        txtLvlHSFastestTimeName.text = "Jack";
        txtLvlHSFastestTimeVal.text = "03:45";

        // txtbigBallScore.text = Mathf.Round((GameManager.GMinstance.bigBallScore*10)/10).ToString();
        // txtbigCubeScore.text = Mathf.Round(GameManager.GMinstance.bigCubeScore*10/10).ToString();
        // txtbigCylinderScore.text = Mathf.Round(GameManager.GMinstance.bigCylinderScore*10/10).ToString();
        // txtbigPyramidScore.text = Mathf.Round(GameManager.GMinstance.bigPyramidScore*10/10).ToString();
        // txtsmallBallScore.text = Mathf.Round(GameManager.GMinstance.smallBallScore*10/10).ToString();
        // txtsmallCubeScore.text = Mathf.Round(GameManager.GMinstance.smallCubeScore*10/10).ToString();
        // txtsmallCylinderScore.text = Mathf.Round(GameManager.GMinstance.smallCylinderScore*10/10).ToString();
        // txtsmallPyramidScore.text = Mathf.Round(GameManager.GMinstance.smallPyramidScore*10/10).ToString();

    }
}
