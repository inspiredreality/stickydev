﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StickyScoreboard : MonoBehaviour
{
    // public TextMeshProUGUI txtStickyScore, txtBallsLost, txtRealTime;
    public TextMeshPro txtStickyScore, txtBallsLost, txtRealTime,
    txtbigBallScore, txtbigCubeScore, txtbigCylinderScore, txtbigPyramidScore, txtsmallBallScore,
     txtsmallCubeScore, txtsmallCylinderScore, txtsmallPyramidScore, txtPlayerPlaying;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // (TextMeshPro)
        txtPlayerPlaying.text = LeaderboardTracking.LTinstance.playingPlayer + "'s Score";
        txtStickyScore.text = GameManager.GMinstance.totalScore;
        txtBallsLost.text = "-" + (GameManager.GMinstance.ballsLostRaw*10).ToString();
        txtRealTime.text = GameManager.GMinstance.realTime;

        txtbigBallScore.text = Mathf.Round((GameManager.GMinstance.bigBallScore*10)/10).ToString();
        txtbigCubeScore.text = Mathf.Round(GameManager.GMinstance.bigCubeScore*10/10).ToString();
        txtbigCylinderScore.text = Mathf.Round(GameManager.GMinstance.bigCylinderScore*10/10).ToString();
        txtbigPyramidScore.text = Mathf.Round(GameManager.GMinstance.bigPyramidScore*10/10).ToString();
        txtsmallBallScore.text = Mathf.Round(GameManager.GMinstance.smallBallScore*10/10).ToString();
        txtsmallCubeScore.text = Mathf.Round(GameManager.GMinstance.smallCubeScore*10/10).ToString();
        txtsmallCylinderScore.text = Mathf.Round(GameManager.GMinstance.smallCylinderScore*10/10).ToString();
        txtsmallPyramidScore.text = Mathf.Round(GameManager.GMinstance.smallPyramidScore*10/10).ToString();
    }
}
