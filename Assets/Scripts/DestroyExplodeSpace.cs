using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyExplodeSpace : MonoBehaviour
{
    public int delayDestroySecs = 2;
    float fade = 0f, fadePace = 0.17f;
    private Material m_material;
    public Color ballColor, edgeColor;

    void Awake()
    {
        m_material = GetComponent<Renderer>().material;
    }
    void FixedUpdate()
    {
        //start countdown
        // StartCoroutine(SpawnDestroyerTimer());

        //fade this frame
        fade += (Time.deltaTime/delayDestroySecs);
        m_material.SetFloat("_Fade", fade);

        // change color
        ballColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        edgeColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        m_material.SetColor("_Color", ballColor);
        m_material.SetColor("_EdgeColor", edgeColor);

    }
    // IEnumerator SpawnDestroyerTimer()
    // {
    //     // audioData.Play();
    //     yield return new WaitForSeconds(delayDestroySecs);
    //     Destroy(gameObject);
    // }
}
